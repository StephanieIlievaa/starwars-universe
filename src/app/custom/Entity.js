/**
 *Requirements - name and data properties
*/
export default class Entity {
    constructor(name, data) {
      this.name = name;
      this.data = data;
    }
  }
  
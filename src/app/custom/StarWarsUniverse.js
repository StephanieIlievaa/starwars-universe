import Entity from './Entity.js';

/**
 *Requirements - E6 class
*/
export default class StarWarsUniverse {
  constructor() {
    this.entities = [];
  }

  async getApi(url) {
    const res = await fetch(url);
    const data = await res.json();

    return data;
  }
  async init() {
    const baseUrl = 'https://swapi.boom.dev/api/';

    const response = await this.getApi(baseUrl);

    for (const [k, v] of Object.entries(response)) {
      const dataEntity = await this.getApi(v);

      this.entities.push(new Entity(k, dataEntity));

      /**
       * * console.info(this.entities);  */
       
    }
  }
}